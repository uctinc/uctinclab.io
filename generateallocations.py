# This file was used to generate data/allocation.toml with all free entries.

import ipaddress

full_subnet = ipaddress.IPv4Network("172.24.0.0/13")

with open("data/allocation.toml", "w") as f:
    for subnet in full_subnet.subnets(new_prefix=22):
        f.write("[[ allocation ]]\n")
        f.write("Subnet = \"{}\"\n".format(subnet))
        f.write("Owner = \"{}\"\n".format("Free"))
        f.write("\n")
