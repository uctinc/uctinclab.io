uctinc.net is an organized collection of Tinc peers with the purpose of being
connected.

While the core group of peers is located around the University of Cincinnati,
anyone may register and peer to any other peers on the network. The network is
managed by volunteers with the sole purpose of routing things. Membership and
allocations are completely free and open, assuming you are able to peer to an
existing peer, and follow rules regarding fair network use.

The network was made for the following uses:

+ Ability to connect to devices behind an uncontrolled NAT
+ Secure interconnect between homelabs for remote control, backup transfers,
  etc.
+ Safe host staging area, networking test area
+ Stable 'static' addresses addressable from anywhere within the network
+ Redundant routing of traffic

For more information on peering, check out the [peering](./page/peering) page.

For more information on membership, check out the [membership](./page
membership) page.

For more information on allocations, check out the
[allocations](./page/allocations) page.

For more information on programmatic access to the membership and allocation
records, check out the [api](./page/api) page.
