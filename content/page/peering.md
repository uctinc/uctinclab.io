---
title: Peering with UCTinc
subtitle: Adding a UCTR to the network
comments: false
---

UCTinc has no requirements of membership in order to peer. If you do peer, we do
ask that you follow the responsibilities outlined under [UCTR
responsibilities](../responsibilities#uctr-responsibilities). Many members are
very happy and more than willing to peer with you. Reach out to a current member
to get peering set up and connected to the network! You can find a list of
members on the [membership](../membership) page.

Peers owned by UCTinc members connecting to another peer on the UCTinc network
must be formatted in the following manner `UCTMxxxName` where `UCTMxxx` is your
`MemberID`, and `Name` is any alpha numeric unique identifier. `Name` should be
upper case, and should avoid non-standard special characters.

Peers not owned by a UCTinc member must NOT attempt to name their peer like a
peer owned by a UCTinc member. Instead, a peer owned by a non-member should be
named in the following format `GUESTNODEXXXX` where `XXXX` is a random
identifier which you create. Make sure `XXXX` is not a current identifier on the
network, as all peer names must be unique.

UCTinc has a set of public peers - anyone may add their tinc installation to
this list (in the form of a merge request) as long as their server is routeable
over the public Internet.

Below is a list of public peers:

{{% publicpeerstable %}}

You will still have to contact these owners if you plan to peer with them so
they can add your peer!

### Adding your public peer

You may add your peer to this table by adding it to the
`./data/publicpeers.toml` file. Each entry is sorted first by owner `MemberID`
alphabetically, then by `hostname` alphabetically. Each entry must be of the
following format:

```
[[ peer ]]
Owner = "UCTMxxx"
Identifier = "ROUTER"
Address = "domain_name.orIPAddress.com"
SubnetBehind = "172.x.x.0/22"
```

Where

+ `Owner` is your `MemberID`.
+ `Identifier` is the second part of your peer identifier.
+ `Address` is the publicly routeable address for a peer to reach your server.
+ `SubnetBehind` is the subnet located behind the given router.

Your peer name should be `MemberIDIdentifier`. This is automatically rendered
by this page when you enter your peer into the `./data/publicpeers.toml` file.

In addition to creating your entry, put a copy of your __public__ key within the
`./static/pubkeys` folder with the filename format matching exactly
`UCTMxxxIdentifier.pub` where `UCTMxxx` is your `MemberID`, and `Identifier` is
the identifier of the UCTR. For example, `UCTM001HOME.pub` is a valid filename.
