---
title: Membership
subtitle: Membership details, Membership list
comments: false
---

UCTinc members are able to own allocations on the UCTinc network.

To become a member, open a [merge
request](https://gitlab.com/uctinc/uctinc.gitlab.io/merge_requests) on the
[uctinc.gitlab.io](https://gitlab.com/uctinc/uctinc.gitlab.io) repository.

The pull request may contain other changes, but for membership must contain an
additional entry within the `./data/membership.toml` file with the following format:

```
[[ membership ]]
MemberID=UCTMxxx
Identity=MyLegal Name
ContactEmail=me@somewhere.com
NetworkOperator=false
```

The membership has the following requirements:

+ Membership entries within the `./data/membership.toml` file must be sorted
  from smallest to largest `MemberID`.
+ `MemberID`s must follow the `UCTMxxx` format, where `xxx` is any three digit
  numeric number with left padded zeros if required.
+ `Identity` must be a legal entity name for which you are authorized to
  represent.
+ `ContactEmail` must be a valid email reachable on the public Internet. It is
  used for peering negotiation between members as well as a communication
  channel for when responsibilities of an allocation or UCTR are broken.
+ `NetworkOperator` indicates operator status as either a `true` or `false`
  conditional.

Once the merge request is approved by 1/2 of the network operators, your merge
request will be merged into the main branch, documenting your membership.

### Changing membership information

Membership information may be changed at any time via a [merge
request](https://gitlab.com/uctinc/uctinc.gitlab.io/merge_requests) to the
[uctinc.gitlab.io](https://gitlab.com/uctinc/uctinc.gitlab.io) repository by the
member in question. Once approved and merged, the information will be updated.
If changing `MemberID`, all allocations must also be transferred. Any subnets
using a FQDN reserved for the old `MemberID` must be updated.

### Removing membership

Membership of any member may be removed at any time via a [merge
request](https://gitlab.com/uctinc/uctinc.gitlab.io/merge_requests) to the
[uctinc.gitlab.io](https://gitlab.com/uctinc/uctinc.gitlab.io) repository. You
may remove membership of any legal entity which you are authorized to represent
for any reason, and may remove membership of any other legal entity which is
breaking any allocation or UCTR [responsibilities](../responsibilities). When
removing membership, make sure to transfer all owned allocations beforehand, or
to free them within the merge request.

## Membership list

Below is the generated list of current members:

{{% membershiptable %}}

Bold MemberIDs signify network operators.
