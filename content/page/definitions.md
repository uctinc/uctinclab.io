---
title: Definitions
subtitle: Official definitions used throughout the website
comments: false
---

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD",
"SHOULD NOT", "RECOMMENDED",  "MAY", and "OPTIONAL" in this document are to be
interpreted as described in [RFC 2119](https://tools.ietf.org/html/rfc2119).

Below is the list of definitions for the network:

+ **Allocation** - An allocation is a set of IP addresses which may be
  controlled by one or more members.
+ **Member** - A member is a singular legal entity, whether a business or
  person.
+ **Network operator** - A network operator is a member who is able to approve
  pull requests from the UCTinc Gitlab.
+ **POP** - A POP (Point of presence) designates a singular physical location to
  which traffic may be routed to.
+ **Host** - A host is any system connected to any network interface able to
  route traffic over it.
+ **Public Host** - A public host is a host which has an address routable over
  the Internet.
+ **Nonpublic Host** - A nonpublic host is a host which is able to route traffic
  over the Internet, but does not have a publicly routable address. This is
  common in hosts located behind another router providing NAT.
+ **UCT Host** - A UCT host is a host which has an address which is routable
  from the UCT network, and is able to route traffic back onto the UCT network.
+ **Router** - A router is a host which is capable of forwarding traffic between
  two independent networks.
+ **UCTR** - A UCTR (UCTinc Router) is a Router which is able to route traffic
  between a non-UCTinc network and the UCTinc network. It is also able to route
  traffic between two different UCTRs. It is able to advertise address ranges to
  the rest of the UCTinc network. A UCTR is not necessarily a UCT Host.
+ **Peer** - A peer is a direct routing relationship between one UCTR and
  another UCTR.
