---
title: Allocations
subtitle: Subnets, Ownership, Allocation Details, and Allocation List
comments: false
---

UCTinc collectively sits on the 172.24.0.0/13 subnet. This means that the subnet
spans inside of a privately routed range from address 172.24.0.0 to address
172.31.255.255. This range was chosen because it is located within the private
address range defined by [RFC1918](https://tools.ietf.org/html/rfc1918), and
between all private ranges was deemed to be the least likely to cause collisions
with existing networks.

UCTinc is divided into a set of aligned /22 address allocations. This then means
that there are 512 possible allocations. Each allocation exists in one of the
following states:

+ **Reserved** - This range of addresses is not available for allocation by
  members.
+ **Free** - This range of addresses is available to be allocated to a member.
  Until allocated, this address range should not be advertised by any UCTR.
+ **Allocated** - This range of addresses is being managed by member, and should
  not be advertised by any UCTRs except UCTRs controlled by the owning member.

### Allocating an allocation

Any member may request allocation of any subnet currently a "Free" state. In
order to gain ownership over the allocation, a [merge
request](https://gitlab.com/uctinc/uctinc.gitlab.io/merge_requests) must be
opened on the [uctinc.gitlab.io](https://gitlab.com/uctinc/uctinc.gitlab.io)
repository. While your merge request may contain other changes (such as
membership), in order to be granted an allocation, your desired allocation must
have updated information in the `./data/allocation.toml` file with the following
format:

```
[[ allocation ]]
Subnet = "172.x.x.0\22"
Owner = "UCTMxxx"
SubnetName = "somename.uctinc.net"
IntendedUse = "What are you going to use this for?"
```

The allocation entry has the following requirements:

+ Allocations within the `./data/allocation.toml` must be sorted from smallest
  to largest base subnet address.
+ Allocated subnets must have had `Owner` be set to `Free` to signify that it is
  a subnet which can be allocated, or set to the old owner's `MemberID` in the
  case of transferring ownership to a different member.
+ `Owner` must be the new owner's `MemberID`. If the previous `Owner` was
  `Free`, this is the merge request owner's `MemberID`. If the previous `Owner`
  was the old author, then the `Owner` field must be set to the `MemberID` of
  the new owner. The old owner must signify that they accept the transfer in
  some way within the merge request.
+ `SubnetName` must be a FQDN that follows the following rules:
  1. The FQDN must end in `uctinc.net`.
  2. The FQDN must conform to [RFC952](https://tools.ietf.org/html/rfc952).
  3. Alpha characters within the FQDN must be lowercase.
  3. The FQDN must not already be used for a different subnet.
  4. The head hostname portion of the FQDN must be at least 5 characters long.
     For example, `longsecondary.a.uctinc.net` is not legal because `a` is less
     than 5 characters long. `hostn.uctinc.net` is valid since `hostn` is 5
     characters long.
  5. The FQDN should not represent a subhost of an already allocated subnet
     which the `Owner` does not already own. For example, if UCTM998 owns
     `hello.uctinc.net`, UCTM999 should not in turn register
     `hi.hello.uctinc.net`. However, UCTM998 may register `hi.hello.uctinc.net`
     since UCTM998 owns `hello.uctinc.net`.
  6. The FQDN should not respresent a superhost of an already allocated subnet
     which the `Owner` does not already own. For example, if UCMT998 owns
     `hi.hello.uctinc.net`, UCTM999 should not in turn register
     `hello.uctinc.net` since `hi.hello.uctinc.net` is already owned by UCMT998.
  7. The head hostname portion of the FQDN should not follow the format of
     `uctmxxx` where x is any numeric character, unless `uctmxxx` is your
     `MemberID`. For example, UCMT999 may register `ucmt999.uctinc.net` or
     `hi.ucmt999.uctinc.net`, however should not register `ucmt998.uctinc.net`
     or `hi.ucmt998.uctinc.net`, since they are reserved for UCMT998.
+ `IntendedUse` should be an explanation of what the subnet will be used for.

Your allocation will be allocated for your use once 1/2 of the network operators
approve your merge request and merge your changes.

### Releasing an allocation

An allocation may be released back to the allocation pool by creating a merge
request where `Owner` is reset to `Free`, `SubnetName` deleted, and
`IntendedUse` deleted. You must only release allocations you own or of members
who's membership is being removed due to breaking allocation or UCTR
[responsibilities](../responsibilities).

### Transferring an allocation

An allocation you own may be transferred to a different member through a merge
request. The new owner must open the merge request with any fields changed, and
must have the `Owner` field changed to their `MemberID`. The old owner must
signify within the merge request comments that they accept the transfer. Once
the merge request is approved and merged, the ownership is updated.

### Changing information on an allocation

Any other information within an allocation entry may be changed at any time by
the owner through a merge request. Once the merge request is approved and
merged, the information is updated.

## Current Allocation State

### Reserved Allocations

The following allocations are reserved and may not be allocated to a member.

{{% reservedallocations %}}

### Owned Allocations

The following allocations are currently be used by a member.

{{% allocationtable %}}

### Free Allocations

The following allocations may be allocated to a member.

{{% freeallocations %}}
