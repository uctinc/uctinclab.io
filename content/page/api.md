---
title: Api
subtitle: Programmatic access to registration data
comments: false
---

UCTinc's registration data is set up to allow for programmatic access to its
registration data.

### Membership data access

Membership data for UCTinc is stored in `./data/membership.toml` from within the
[uctinc.gitlab.io](https://gitlab.com/uctinc/uctinc.gitlab.io) repository. You
may either:

1. Query Gitlab itself for the latest file
2. Clone the repository

For historic data, you should use a git checkout to view previous versions of
database.

Membership data is organized in the toml file format in the following manner:

```
[[ membership ]]
MemberID = "UCTMxxx"
Identity = "Identity Name"
ContactEmail = "contact@email.com"
NetworkOperator = true
```

All of the fields above must always be present for each entry. For more
information on what each of these fields represent, see the
[membership](../membership) page.

### Allocation data access

Allocation data for UCTinc is stored in `./data/allocation.toml` from within the
[uctinc.gitlab.io](https://gitlab.com/uctinc/uctinc.gitlab.io) repository. You
may either:

1. Query Gitlab itself for the latest file
2. Clone the repository

For historic data, you should use a git checkout to view previous versions of
database.

Allocation data is organized in the toml file format in the following manner:

```
[[ allocation ]]
Subnet = "subnet"
Owner = "UCTMxxx"
SubnetName = "something.uctinc.net"
IntendedUse = "SomeUse"
```

If `Owner` is `Free`, only `Subnet` and `Owner` fields will be present. If
`Owner` is `Reserved`, only `Subnet`, `Owner`, and `IntendedUse` fields will be
present. If `Owner` is a `MemberID`, all fields will be present. All subnets
available to be allocated will always be within the database. For more
information on what each of these fields represent, see the
[allocations](../allocations) page.

### Public peer data access

Public peer data for UCTinc is stored in `./data/publicpeers.toml` from within
the [uctinc.gitlab.io](https://gitlab.com/uctinc/uctinc.gitlab.io) repository.
You may either:

1. Query Gitlab itself for the latest file
2. Clone the repository

For historic data, you should use a git checkout to view previous versions of
database.

Public peer data is organized in the toml file format in the following manner:

```
[[ peer ]]
Owner = "UCTMxxx"
Identifier = "ID"
Address = "addr.uctinc.net"
SubnetBehind = "172.24.0.0/22"
```

All of the fields above must always be present for each entry. For more
information on what each of these fields represent, see the
[peering](../peering) page.
