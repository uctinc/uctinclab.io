---
title: Responsibilities
subtitle: Rules as members of the UCTinc network
comments: false
---

## Allocation responsibilities

As the owner of an allocation, a member has the following responsibilities:

+ All addresses and UCT hosts routing using addresses within the allocation are
  the responsibility of member.

## UCTR responsibilities

As an operator of a UCTR, a member has the following responsibilities:

+ A UCTR will not advertise subnets which are not either:
  1. Owned by the member operating the UCTR, or
  2. Located within another member's allocation (allocation owner) which have
     approved for use by the allocation owner for advertisement by a specific
     UCTR not controlled by the allocation owner.
+ A UCTR will not attempt to route traffic with a Source IP address which is not
  either:
  1. Advertised by the UCTR
  2. Routed to the UCTR by another UCTR

+ A UCTR will not attempt to do deep packet inspection on or try to modify any
  traffic which it is routing for another UCTR.  This does not apply to traffic
  originating or traveling to addresses under the following conditions:
  1. Owned by the member operating the UCTR, or
  2. Located within another member's allocation (allocation owner) which have
     approved for use by the allocation owner for monitoring by a specific UCTR
     not controlled by the allocation owner.
  Packet inspection must be limited to information gathered at the transport
  layer (Layer 4 in the OSI model) and below. Packet inspection above layer 4 is
  classified as deep packet inspection and is forbidden as outlined above.

+ A UCTR may refuse to route any traffic between two peer UCTRs categorized
  using packet inspection as long as the inspection does not classify as deep
  packet inspection. Traffic that is dropped at the UCTR layer MUST reply with a
  Destination Unreachable Message ICMP packet to the source. The Destination
  Unreachable Message ICMP is defined in
  [RFC792](https://tools.ietf.org/html/rfc792). The setting to achieve this in
  firewalls is typically named "REJECT", and not "DROP".

+ A UCTR may be held eventually responsible by peers of the UCTR when another
  peer of the UCTR does not conform to the above rules.

